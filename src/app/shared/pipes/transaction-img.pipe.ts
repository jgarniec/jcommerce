import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'transactionImg'
})
export class TransactionImgPipe implements PipeTransform {

  transform(value: unknown): string {
   if (!!value && typeof value === 'string') {
     return `/assets/img/icons/${value.toLowerCase().replace(/\s+/g, '-')}.png`;
   }

   return '';
  }

}
