import { Injectable } from '@angular/core';
import { TransactionModel } from '../models/transaction.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionAdapterService {

  public adapt({ dates: { valueDate }, ...rest }: TransactionModel): TransactionModel {
    return {
      dates: {
        valueDate: typeof valueDate !== 'number' ? new Date(valueDate).getTime() : valueDate,
      },
      ...rest
    };
  }
}
